<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('contacto', function () {
	return '<h1>Contacto</h1>';
});

Route::post('contacto', function () {
	return 'post';
});

//producto/celular

Route::get('productos/{categoria}/listado', function ($categoria) {
	return $categoria;
});


Route::get('producto/{prod}', function ($p) {
	return $p;
});

Route::get('categorias/{categoria}/{subcategoria?}', function ($a, $b='valor por defecto') {
	return $a . ' - ' . $b;
});

Route::get('faq', function () {
	return view('faq');
});

Route::get('resultado/{num1}/{num2?}', 'EjerciciosController@resultado');
Route::get('peliculas', 'PeliculaController@mostrarPelis');
Route::get('pelicula/{id}', 'PeliculaController@show');

Route::get('buscar/{q}', 'EjerciciosController@buscar');


Route::get('eloquent/all', 'EloquentController@all');
Route::get('eloquent/queriesSimples', 'EloquentController@queriesSimples');
Route::get('eloquent/queriesComplejas', 'EloquentController@queriesComplejas');


