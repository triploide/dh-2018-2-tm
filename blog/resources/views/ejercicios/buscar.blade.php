@extends('app')

@section('title', 'Buscar')

@section('main')
	<h1>Resultado de búsqueda</h1>
	<p>{{$resultado}}</p>
@endsection
