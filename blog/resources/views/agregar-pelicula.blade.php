<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Agregar Película</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
	<h1>Agregar Película</h1>
	<form action="ruta" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Título</label>
            <input type="text" name="title" id="title"/>
        </div>
        <div class="form-group">
            <label for="rating">Rating</label>
            <input type="text" name="rating" id="rating"/>
        </div>
        <div class="form-group">
            <label for="awards">Premios</label>
            <input type="text" name="awards" id="awards"/>
        </div>
        <div class="form-group">
            <label for="length">Duración</label>
            <input type="text" name="length" id="length"/>
        </div>
        <div class="form-group">
            <label for="release_date">Fecha de Estreno</label>
            <input type="date" name="release_date" id="release_date">
        </div>
        <div class="form-group">
        	<button class="btn btn-primary" type="submit" name="button">Agregar</button>
        </div>
    </form>
</body>
</html>