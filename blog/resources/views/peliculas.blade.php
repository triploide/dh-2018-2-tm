@extends('app')

@section('resources')
	<link rel="stylesheet" type="text/css" href="/css/app.css">
@endsection

@section('main')
	<?php foreach($peliculas as $pelicula): ?>
		<p><?php echo $pelicula['titulo'] ?></p>
	<?php endforeach ?>

	@if (!empty($peliculas))
		@foreach($peliculas as $pelicula)
			<p>{{ $pelicula['titulo'] }}</p>
		@endforeach
	@else
		<p>No hay pelis</p>
	@endif

	@forelse ($peliculas as $pelicula)
		<p>{{ $pelicula['titulo'] }}</p>
	@empty
		<p>No hay pelis</p>
	@endforelse
@endsection

	