<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title', 'RAM')</title>
	@yield('resources')
</head>
<body>
	<header>
		<nav>
			<ul>
				<li><a href="#">Uno</a></li>
				<li><a href="#">Dos</a></li>
				<li><a href="#">Tres</a></li>
			</ul>
		</nav>
	</header>

	<main>
		@yield('main')
	</main>

	<footer>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
	</footer>

	@yield('scripts')
</body>
</html>