<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeliculaController extends Controller
{
	private $peliculas = [
		1 => [
			'titulo' => 'Avatar',
			'premios' => 7
		],
		2 => [
			'titulo' => 'Titanic',
			'premios' => 11
		]
	];

    public function mostrarPelis()
    {
    	return view('peliculas', ['peliculas' => $this->peliculas]);
    }

    public function show($id)
    {
    	$titulo = $this->peliculas[$id]['titulo'];
    	$premios = $this->peliculas[$id]['premios'];

    	return view('pelicula', ['title' => $titulo, 'premios' => $premios]);
    }
}
