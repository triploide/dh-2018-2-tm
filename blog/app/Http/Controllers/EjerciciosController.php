<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EjerciciosController extends Controller
{

	private $constante = 10;

	private $pelis = [
		1 => 'moon',
		2 => 'triangle',
		3 => 'coherence'
	];

    public function resultado($num1, $num2=null)
    {
    	if ($num2 === null) {
			$resultado = ($num1 % 2) ? 'impar' : 'par';
		} else {
			$resultado = $num1 * $num2 * $this->constante;
		}
		return $resultado;
    }

    public function buscar($q)
    {
    	$resultado = 'No hay pelis';
    	foreach ($this->pelis as $peli) {
    		if ($peli == $q) {
    			$resultado = $peli;
    			break;
    		}
    	}
    	return view('ejercicios/buscar', ['resultado' => $resultado]);
    }
}
