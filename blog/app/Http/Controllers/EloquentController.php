<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actor;
use App\Movie;

class EloquentController extends Controller
{
    public function all()
    {
    	$actors = Actor::all();
    	dd($actors);
    }

    public function queriesSimples()
    {
    	//recupero con get - obtengo array de instancias
    	$actors = Actor::where('rating', '>',8)
    		->orderBy('rating', 'desc')
    		->limit(1)
    		->get()
    	;

    	//recupero con first - obtengo una única instancia
    	$actor = Actor::where('rating', '>',8)
    		->orderBy('rating', 'desc')
    		->limit(1)
    		->first()
    	;

    	dd($actor->toArray());
    }

    public function queriesComplejas()
    {
    	$movies = Movie::where(function ($query) {
    		$query->where('release_date', '>=', '2008-01-01')
    			->where('release_date', '<=', '2010-12-31');
    	})->orWhere(function ($query) {
    		$query->where('rating', '>=', 8)
    			->where('rating', '<=', 10);
    	})->get();
    	dd($movies->toArray());
    }

    //leftjoin('tabla', 'column1', 'operador', 'column2')
}
