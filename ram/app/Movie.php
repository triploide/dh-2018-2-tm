<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','rating','release_date','length','awards','genre_id', 'banner'];

    protected $dates = ['release_date', 'deleted_at'];


    /*
    Para recuperar la relación la llamamos como propiedad y no como método
    */
    public function genre()
    {
    	//return $this->belongsTo(Genre::class, 'foreing_key', 'id');
    	return $this->belongsTo(Genre::class);
    }

    public function actors()
    {
        //return $this->belongsToMany(Actor::class, 'tabla_pivot', 'peli_id', 'act_id');
    	return $this->belongsToMany(Actor::class);
    }
    
}
