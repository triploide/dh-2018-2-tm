<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class CartController extends Controller
{
    public function add($id)
    {
    	session()->push('cart', $id);
    	return redirect('cart/show');
    }

    public function show()
    {
    	$movies = Movie::whereIn('id', session()->get('cart'))->get();
    	$movies->dd();
    }

    public function checkout()
    {
    	$movies = Movie::whereIn('id', session()->get('cart'))->get();

    	$purchase = Purchase::create([
    		'date' => date('Y-m-d')
    	]);

    	foreach ($movies as $movie) {
    		$item = new Item([
    			'product_id' => $movie->id,
    			'price' => $movie->price
    		]);
    		$purchase->items()->save($item);
    	}
    	$purchase->items()->save($item);

    	/*
    	$items = collect();
    	foreach ($movies as $movie) {
    		$item = new Item([
    			'product_id' => $movie->id,
    			'price' => $movie->price
    		]);
    		$items->push($item);
    	}
    	$purchase->items()->saveMany($items);
    	*/

    	return redirect('cart/success');
    }

    public function session()
    {
    	session()->put('clave', 'valor'); //guarda el valor
    	session()->push('clave', 'valor'); //guarda el valor en un array
    	session()->get('clave'); //devuelve un valor
    	session()->forget('clave'); //borra la clave y su valor de session
    	session()->flush(); //borra todo de session
    }
}
