<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function index()
    {
    	$genres = Genre::all();
    	return view('genres.index', compact('genres'));
    }

    public function show($id)
    {
    	//with
    	$genre = Genre::where('id', $id)->with('movies')->first();
    	return view('genres.show', ['genre' => $genre]);
    }
}
