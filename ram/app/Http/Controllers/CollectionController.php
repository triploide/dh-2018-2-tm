<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Genre;

class CollectionController extends Controller
{
    public function map()
    {
    	$movies = Movie::all();
    	$saltoDeLinea = '<br>';

    	$movies->map(function ($item) use ($saltoDeLinea) {
    		echo $item->title . $saltoDeLinea;
    	});
    }

    public function filter()
    {
    	$movies = Movie::all();

    	$moviesFiltradas = $movies->filter(function ($item) {
    		return $item->rating >= 8;
    	});

    	$moviesFiltradas->dd();
    }

    public function pluck()
    {
    	$genres = Genre::all();

    	dd($genres->pluck('name', 'id')->toArray());
    }

    public function implode()
    {
    	$genres = Genre::all();

    	dd($genres->pluck('name')->implode(', '));
    }

    public function count()
    {
    	$movie = Movie::find(1);

    	dd($movie->actors->count());
    }
}
