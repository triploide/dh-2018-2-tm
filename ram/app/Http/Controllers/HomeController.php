<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Movie;

class HomeController extends Controller
{
    public function index()
    {
        $genres = Genre::orderBy('name')->limit(3)->get();
        $movies = Movie::orderBy('release_date', 'desc')->orderBy('rating', 'desc')->limit(2)->get();
        return view('front.home', ['generos' => $genres, 'pelis' => $movies]);

    }
}
