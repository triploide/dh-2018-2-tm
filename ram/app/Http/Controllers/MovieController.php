<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $movies = Movie::whereNotNull('genre_id')
            ->orderBy('title')
            ->paginate(25)
        ;
    	return view('movies/index', ['movies' => $movies]);
    }

    public function show($id)
    {	
        \App::setLocale('es');
    	$movie = Movie::findOrFail($id);

    	return view('movies/show', ['movie' => $movie]);
    }

    public function create()
    {
        $actors = \App\Actor::all();
        $genres = \App\Genre::all();
    	return view('movies.create', compact('actors', 'genres'));
    }

    public function request()
    {
    	//devuelve el valor de un input, como parámetro recibe el name del input
    	//return request()->input('release_date');

    	//devuelve true o false si el input exsite
    	//dd(request()->has('title'));
    	
    	//devuelve el request en formato array
    	//dd(request()->all());

    	//dd(request()->except('_token', 'button'));
    	
    	dd(request()->only('title', 'rating', 'query'));

    }

    public function store()
    {

        //request()->file('banner')->store('movies');

    	//validar
    	request()->validate([
    		'title' => 'required|string|min:3|max:255|unique:movies',
    		'rating' => 'required|numeric',
            'banner' => 'image|max:2000|dimensions:ratio=4/3'
    	]);

    	//insert en db
    	/*
    	$movie = new Movie;
    	$movie->title = request()->input('title');
    	$movie->rating = request()->input('rating');
    	$movie->release_date = request()->input('release_date');
    	$movie->length = request()->input('length');
    	$movie->awards = request()->input('awards');
    	$movie->save();
    	*/

    	/*
    	update
    	$movie = Movie::find(1);
    	$movie->columna = 'Nuevo Valor';
    	$movie->save();

    	delete
    	$movie = Movie::find(1);
    	$movie->delete();
    	*/

        //Persistencia de la imagen
        $ext = request()->file('banner')->extension();
        $nombre = str_slug(request()->input('title'));
        $nombre = request()->file('banner')->storeAs('movies', $nombre.'.'.$ext);

        $datos = request()->all();
        $datos['banner'] = $nombre;

    	$movie = Movie::create($datos);

        $movie->actors()->sync(request()->input('actors'));

    	dd('Todo bien');
    }

    public function edit($id)
    {
        $movie = Movie::find($id);
        return view('movies.edit', compact('movie'));
    }

    public function update($id)
    {
        $movie = Movie::find($id);
        $movie->update(request()->all());
        dd('Todo bien');

        return redirect('movies');
    }


    public function destroy($id)
    {
    	$movie = Movie::find($id);
        $movie->actors()->sync([]);
    	$movie->delete();
    	return 'Bien';
    }
}
