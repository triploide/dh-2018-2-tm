<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Movie;

class MovieController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api');
	}
	
    public function index()
    {
    	return Movie::all();
    }

    public function show($id)
    {
    	return Movie::find($id);
    }

    public function create()
    {
    	return Movie::create(request()->all())->id;
    }

    public function destroy($id)
    {
    	Movie::find($id)->delete();
    	return ['success' => true];
    }
}
