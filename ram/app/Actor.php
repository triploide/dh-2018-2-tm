<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    public function movies()
    {
        //return $this->belongsToMany(Movie::class, 'tabla_pivot', 'act_id', 'peli_id');
    	return $this->belongsToMany(Movie::class);
    }
}
