const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
*/

/*
mix.styles([
	'resources/css/styles1.css',
	'resources/css/styles2.css'
], 'public/css/custom.css')
*/
/*
mix.styles('resources/css/', 'public/css/custom.css')
	.scripts('resources/js/custom/', 'public/js/custom.js')
	.version();
*/

mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');