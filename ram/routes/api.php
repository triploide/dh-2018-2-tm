<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('movies', 'Api\MovieController@index');
Route::get('movies/{id}', 'Api\MovieController@show');
Route::post('movies', 'Api\MovieController@create');
Route::delete('movies/{id}', 'Api\MovieController@destroy');
