<?php
//----------------
//-----Movies-----
//----------------

//Muestra es listado de todas las películas
Route::get('movies', 'MovieController@index')->middleware('admin');

//Muestra el formulario de creación de una película
Route::get('movies/create', 'MovieController@create')->middleware('auth');

//Crea la película
Route::post('movies', 'MovieController@store');

//Muestra el formulario de edición
Route::get('movies/{id}/edit', 'MovieController@edit');

//Edita la película
Route::put('movies/{id}', 'MovieController@update');

//Muestra una película determinada
Route::get('movies/{id}', 'MovieController@show');

//Borra una película
Route::delete('movies/{id}', 'MovieController@destroy');

//----------------
//-----Genres-----
//----------------
Route::get('generos', 'GenreController@index');
Route::get('generos/{id}', 'GenreController@show');

//---------------------
//-----Collections-----
//---------------------

Route::get('collections/map', 'CollectionController@map');
Route::get('collections/filter', 'CollectionController@filter');
Route::get('collections/pluck', 'CollectionController@pluck');
Route::get('collections/implode', 'CollectionController@implode');
Route::get('collections/count', 'CollectionController@count');

Route::get('auth/ejemplos', 'AuthController@ejemplos');


//---------------
//-----Front-----
//---------------
Route::get('/', 'HomeController@index');
Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index');

//Carrito
Route::get('cart/add/{id}', 'CartController@add');
Route::get('cart/show', 'CartController@show');


//React
Route::view('react', 'react');
