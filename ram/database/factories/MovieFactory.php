<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'rating' => $faker->randomFloat(1, 1, 9),
        'awards' => $faker->numberBetween(1, 10),
        'length' => $faker->numberBetween(40, 210),
        'release_date' => $faker->datetime(),
        'genre_id' => $faker->numberBetween(1, 13)
    ];
});

//factory(App\Movie::class)->make();
