<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('genres')->insert([
        	[
        		'name' => 'Animadas',
        		'ranking' => 13,
        		'active' => 1
        	],
        	[
        		'name' => 'Sitcom',
        		'ranking' => 14,
        		'active' => 0
        	]
        ]);
    }
}
