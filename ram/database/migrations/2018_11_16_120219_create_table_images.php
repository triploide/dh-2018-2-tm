<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->smallIncrements('id'); //auto increment - unique - primary key - unsigned
            $table->string('src', 100)->unique(); //varchar 255
            $table->text('description')->nullable(); //text
            $table->boolean('is_active')->default(1); //0 o 1
            $table->date('schedule');
            $table->softDeletes(); //deleted_at
            $table->timestamps(); //created_at updated_at

            //->index()
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
