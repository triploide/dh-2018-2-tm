@extends('app')

@section('title', 'Películas')

@section('main')
	<h1>Listado de películas</h1>

	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Título</th>
				<th>Género</th>
				<th>Actores</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($movies as $movie)
				<tr>
					<td>{{ $movie->title }}</td>
					<td>{{ $movie->genre->name }}</td>
					<td>{{ $movie->actors->pluck('first_name')->implode(', ') }}</td>
					<td>
						<a class="btn btn-primary" href="/movies/{{$movie->id}}/edit">
							<span class="fa fa-pencil"></span>
						</a>
						<form action="/movies/{{$movie->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger">
								<span class="fa fa-trash"></span>
							</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{{ $movies->links() }}
@endsection