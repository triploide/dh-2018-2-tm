@extends('app')
@section('title', $movie->title)

@section('main')
	<h1>{{ $movie->title }}</h1>

	@if ($movie->banner)
		<img src="/storage/{{$movie->banner}}" class="img-fluid" style="max-width: 300px">
	@endif

	<p>Género: {{ $movie->genre->name }}</p>

	<p><strong>Actores</strong></p>

	@foreach ($movie->actors as $actor)
		<p>{{ $actor->first_name }}</p>
	@endforeach
	
@endsection