@extends('app')
@section('title', 'Crear película')

@section('main')
	<h1>Agregar Película</h1>

	@if (count($errors))
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
		</div>
	@endif

	{{--
		$errors->has('title')
		$errors->first('title')
		$errors->get('title')
	--}}

	<form action="/movies" method="POST" enctype="multipart/form-data">
		@csrf
        <div class="form-group">
            <label for="title">Título</label>
            <input class="form-control" type="text" name="title" id="title" value="{{old('title')}}" />
        </div>
        <div class="form-group">
            <label for="rating">Rating</label>
            <input class="form-control" type="text" name="rating" id="rating"  value="{{old('rating')}}"/>
        </div>
        <div class="form-group">
            <label for="awards">Premios</label>
            <input class="form-control" type="text" name="awards" id="awards"  value="{{old('awards')}}"/>
        </div>
        <div class="form-group">
            <label for="length">Duración</label>
            <input class="form-control" type="text" name="length" id="length"  value="{{old('length')}}"/>
        </div>
        <div class="form-group">
            <label for="release_date">Fecha de Estreno</label>
            <input class="form-control" type="date" name="release_date" id="release_date"  value="{{old('release_date')}}">
        </div>
        <div class="form-group">
            <label for="banner">Banner</label>
            <input class="form-control" type="file" name="banner" id="banner">
        </div>
        <div class="form-group">
            <label for="genre_id">Género</label>
            <select name="genre_id" id="genre_id" class="form-control">
                @foreach ($genres as $genre)
                    <option value="{{$genre->id}}">{{ $genre->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Actores</label>
            <div class="row">
                @foreach ($actors as $actor)
                    <div class="col-xs-4">
                        <label for="{{$actor->last_name}}">{{$actor->last_name}}</label>
                        <input id="{{$actor->last_name}}" type="checkbox" name="actors[]" value="{{$actor->id}}">
                    </div>
                @endforeach
            </div>
            
        </div>
        <div class="form-group">
        	<button class="btn btn-primary" type="submit" name="button">Agregar</button>
        </div>
    </form>
@endsection