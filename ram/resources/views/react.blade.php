<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>React</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
	<div id="example"></div>

	<script src="/js/app.js"></script>
</body>
</html>