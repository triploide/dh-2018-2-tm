@extends('app')

@section('title', $genre->name)

@section('main')
	<h1>{{ $genre->name }}</h1>

	<ul>
		@foreach ($genre->movies as $movie)
			<li>{{ $movie->title }}</li>
		@endforeach
	</ul>

	{{-- $genre->movies->pluck('title')->implode(' - ') --}}
@endsection