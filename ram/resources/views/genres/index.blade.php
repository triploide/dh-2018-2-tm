@extends('app')

@section('title', 'Géneros')

@section('main')
	<ul>
		@foreach ($genres as $genre)
			<li>
				<a href="/generos/{{$genre->id}}">{{ $genre->name }}</a>
			</li>
		@endforeach
	</ul>
@endsection