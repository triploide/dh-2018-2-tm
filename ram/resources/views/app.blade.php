<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/custom.css">
</head>
<body>

	<header>
		<ul class="nav navbar-expand-md navbar-dark fixed-top bg-dark">
		  	<li class="nav-item">
		    	<a class="nav-link active" href="#">{{ __('web.active') }}</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link" href="#">Link</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link" href="#">Link</a>
		  	</li>
		  	<li class="nav-item">
		    	<a class="nav-link disabled" href="#">Disabled</a>
		  	</li>
		  	@auth()
		  		<li class="nav-item">
			    	<a class="nav-link disabled" href="/logout">Logout</a>
			  	</li>
			@else
				<li class="nav-item">
			    	<a class="nav-link disabled" href="/login">Login</a>
			  	</li>
		  	@endauth
		  	
		  	
		</ul>
	</header>

	<main style="margin-top: 40px">
		@yield('main')
	</main>

	<footer class="footer">
	  	<div class="container">
	    	<span class="text-muted">Place sticky footer content here.</span>
	  	</div>
	</footer>

</body>
</html>