<!DOCTYPE html>
<html lang="en">

  @include('front.partials.head')

  <body id="page-top">

    @include('front.partials.nav')

    @include('front.partials.header')

    @yield('main')

    @include('front.partials.footer')

    @include('front.partials.scripts')
    
  </body>

</html>
